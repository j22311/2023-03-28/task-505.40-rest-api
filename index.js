//import thư viện express
const express = require ("express");

//khỡi tạo một app expresss 
const app  = express();

// khai báo cổng chạy project 
const port = 8000;
//
// khai báo đọc app đọc được body JSON 
app.use(express.json());
// khai báo dạng API dạng GET 
app.get("/get-method", (req,res) => {
    res.json({
        message: "GET Method"
    })
})

//khai báo API danjng POSt
app.post("/post-method", (req,res) => {
    res.json({
        message: "POST method"
    })
})

//KHai báo dạng API dạng PUT
app.put("/put-method",(req,res) => {
    res.json({
        message: "PUT Method"
    })
})

//KHai báo dạng API dạng DELETE
app.delete("/delete-method",(req,res) => {
    res.json({
        message: "DELETE Method"
    })
})

//request Params get put delete
//request params xuất hiện trong URL của API
app.get("/request-params/:param1/:param2/param", (req,res) => {
    let param1 = req.params.param1;
    let param2 = req.params.param2;

    res.json ({
        params : {
            param1,
            param2
        }
    })
})

//request Query (chỉ áp dụng cho phương thức get)
//khi lấy request Query thì mình phải validate
app.get("/request-query", (req,res) => {
    // let query = req.query;
    param1 =  [ {
        ID: 1,
        maNuocUong: "TRATAC",
        tenNuocUOng: "Trà tắc",
        donGia: 10000,
        ngayTao: "14/05/2021",
        ngayCapNhat: "14/05/2021"
    },
    {
        ID: 2,
        maNuocUong: "COCA",
        tenNuocUOng: "Cocacola",
        donGia: 15000,
        ngayTao: "14/05/2021",
        ngayCapNhat: "14/05/2021"
    },

    {
        ID: 3,
        maNuocUong: "PEPSI",
        tenNuocUOng: "pepsi",
        donGia: 15000,
        ngayTao: "14/05/2021",
        ngayCapNhat: "14/05/2021"
    }]
    res.json({
        param1
       
    })
})

//request body JSON(chỉ áp dụng cho POST,PUT)
app.post("/request-body-json", (req,res)=> {
    let body = req.body;

    res.json({
        body: body
    })
})

//Task 505.40 Rest API trả lại danh sách đồ uống fix cứng
app.get("/drinks-class", (req,res) => {
    // let query = req.query;
    class1 =  [ {
        ID: 1,
        maNuocUong: "TRATAC",
        tenNuocUOng: "Trà tắc",
        donGia: 10000,
        ngayTao: "14/05/2021",
        ngayCapNhat: "14/05/2021"
    },
    {
        ID: 2,
        maNuocUong: "COCA",
        tenNuocUOng: "Cocacola",
        donGia: 15000,
        ngayTao: "14/05/2021",
        ngayCapNhat: "14/05/2021"
    },

    {
        ID: 3,
        maNuocUong: "PEPSI",
        tenNuocUOng: "pepsi",
        donGia: 15000,
        ngayTao: "14/05/2021",
        ngayCapNhat: "14/05/2021"
    }]
    res.json({
        class1
       
    })
})

app.get("/drinks-object", (req,res) => {
    // let query = req.query;
    class1 =   {
        ID: 1,
        maNuocUong: "TRATAC",
        tenNuocUOng: "Trà tắc",
        donGia: 10000,
        ngayTao: "14/05/2021",
        ngayCapNhat: "14/05/2021"
    };
    class2 = {
        ID: 2,
        maNuocUong: "COCA",
        tenNuocUOng: "Cocacola",
        donGia: 15000,
        ngayTao: "14/05/2021",
        ngayCapNhat: "14/05/2021"
    };

    class3 = {
        ID: 3,
        maNuocUong: "PEPSI",
        tenNuocUOng: "pepsi",
        donGia: 15000,
        ngayTao: "14/05/2021",
        ngayCapNhat: "14/05/2021"
    }
    res.json({
        class1,class2,class3
       
    })
})
// cổng lắng nghe 
app.listen(port, () => {
    console.log("App listening on port: ", port);
})